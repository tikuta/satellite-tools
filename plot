#!/usr/bin/env python
# coding=utf-8

from __future__ import division, print_function, unicode_literals
import sys
import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt
from argparse import ArgumentParser
import numpy as np
import json
import os
import seaborn as sns
from collections import OrderedDict

sns.set_style("ticks")

def moving_average(x, y, n):
    xx = np.cumsum(x, dtype=np.float)
    xx[n:] = xx[n:] - xx[:-n]
    xx = xx[n-1:] / n

    yy = np.cumsum(y, dtype=np.float)
    yy[n:] = yy[n:] - yy[:-n]
    yy = yy[n-1:] / n

    return xx[::n], yy[::n]


def plot(input_file, label='', xscale=1.0, yscale=1.0, skip=None, alpha=1):
    data = np.loadtxt(input_file, delimiter=',')

    if len(data.shape) == 1:
        xx = np.arange(len(data)) * xscale
        yy = data * yscale
    else:
        xx = data.T[0] * xscale
        yy = data.T[1] * yscale

    if skip and len(xx) > skip:
        xx, yy = moving_average(xx, yy, skip)

    plt.plot(xx, yy, label=label, alpha=alpha)

if __name__ == '__main__':
    parser = ArgumentParser(description='graph plotter with matplotlib')
    parser.add_argument('input', nargs='+', type=str, help='CSV path to input')
    parser.add_argument('-o', '--output', nargs=1, type=str, default=['graph.png'], help='graph output path, default: graph.png')
    parser.add_argument('-x', '--xlabel', nargs=1, type=str, default=[None], help='xlabel string')
    parser.add_argument('-y', '--ylabel', nargs=1, type=str, default=[None], help='ylabel string')
    parser.add_argument('--xlim', nargs=1, type=str, default=[None], help='xrange separated with ":", like XMIN:XMAX')
    parser.add_argument('--ylim', nargs=1, type=str, default=[None], help='yrange separated with ":", like YMIN:YMAX')
    parser.add_argument('--xscale', nargs=1, type=str, default=['1.0'], help='xscale to be multiplied')
    parser.add_argument('--yscale', nargs=1, type=str, default=['1.0'], help='yscale to be multiplied')

    parser.add_argument('--moving-average', dest='skip', nargs=1, type=int, default=[None], help='shorten the data with moving average')

    parser.add_argument('--title', nargs=1, type=str, default=[''], help='graph title, default: output filename')

    parser.add_argument('--json', dest='json', action='store_true', help='treat arguments as JSON')
    parser.add_argument('--no-json', dest='json', action='store_false', help='treat arguments as a list of file path, default')
    parser.set_defaults(json=False)

    parser.add_argument('--auto-legend', dest='legend', action='store_true', help='treat filenames as legend, default')
    parser.add_argument('--no-auto-legend', dest='legend', action='store_false', help='do not treat filename as legend')
    parser.set_defaults(legend=True)

    parser.add_argument('--alpha', nargs=1, type=float, default=[1.0], help='alpha value of drawings')

    args = parser.parse_args()

    xscale = float(eval(args.xscale[0]))
    yscale = float(eval(args.yscale[0]))

    if args.json:
        j = json.loads(args.input[0], object_pairs_hook=OrderedDict)
        for k, v in j.items():
            if len(j) > 6:
                with sns.color_palette('husl', len(j)):
                    plot(v, k, xscale=xscale, yscale=yscale, skip=args.skip[0], alpha=args.alpha[0])
            else:
                plot(v, k, xscale=xscale, yscale=yscale, skip=args.skip[0], alpha=args.alpha[0])
        plt.legend(loc=3, bbox_to_anchor=(0., 1.02, 1., .102))
    else:
        for f in args.input:
            if len(args.input) > 6:
                with sns.color_palette('husl', len(args.input)):
                    plot(f, f, xscale=xscale, yscale=yscale, skip=args.skip[0], alpha=args.alpha[0])
            else:
                plot(f, f, xscale=xscale, yscale=yscale, skip=args.skip[0], alpha=args.alpha[0])
        if args.legend == True and len(args.input) > 1:
            plt.legend(loc=3, bbox_to_anchor=(0., 1.02, 1., .102))

    if args.xlabel[0]:
        plt.xlabel(args.xlabel[0])
    if args.ylabel[0]:
        plt.ylabel(args.ylabel[0])
    plt.title(args.title[0] if args.title[0] else args.output[0])

    if args.xlim[0]:
        plt.xlim([float(v) for v in args.xlim[0].split(':')])
    if args.ylim[0]:
        plt.ylim([float(v) for v in args.ylim[0].split(':')])

    plt.savefig(args.output[0], bbox_inches='tight')
