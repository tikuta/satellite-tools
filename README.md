# Command-line interface graph plotter

## Requirements

* Python2
* matplotlib
* numpy
* seaborn

## File list

For more detail, try `--help` option.

### plot

plot CSV file

### hist

plot histogram

### fsec

pre-treatment of shimadzu HPLC log file for `plot`